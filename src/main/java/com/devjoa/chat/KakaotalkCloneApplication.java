package com.devjoa.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KakaotalkCloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(KakaotalkCloneApplication.class, args);
    }

}
